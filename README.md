# CommonOcean Data - Scenarios

This repository includes scenarios in CommonOcean format. Currently the main data sources are [MarineCadastre](https://marinecadastre.gov/ais/), for AIS data, and [OpenSeaMap](https://www.openseamap.org/index.php?id=openseamap&no_cache=1), which is a subproject of OpenStreetMap, for sea charts.

The scenarios are stored in xml files, which are defined as described in **CommonOcean IO** documentation. An interactive search for scenarios is available at [our site](https://commonocean.cps.cit.tum.de/scenarios).

## Contribute

If you want to contribute with new scenarios, you can simply create a merge request or contact us per [e-mail](mailto:commonocean@lists.lrz.de).
